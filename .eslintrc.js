module.exports = {
  extends: [
    'plugin:vue/recommended',
    'plugin:prettier/recommended',
    'prettier'
  ],
  rules: {
    'prettier/prettier': 'error',
    'vue/no-v-html': "off"
  }
}
